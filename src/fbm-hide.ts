/*
// ==UserScript==
// @name     Facebook Messenger Hide Chat Buttons
// @version  1
// @grant    none
// @match    https://www.messenger.com/*
// @match    https://www.facebook.com/messages/t/*
// @match    https://www.facebook.com/messages/e2ee/t/*
// ==/UserScript==
*/

/* Note to self: DO NOT EDIT THIS in GreaseMonkey; original source found in my
 * bookmarklets folder; edit there and copy here */

/*
  "Last Updated" last update: 2024-01-18

   ==Notes==

   2024-01-18:
   - convert to TS, abandon bookmarking

   2023-08-13:
   - remove a span from chat_name_sel

   2023-03-27:
   - change interval to 2s; something with FBM is causing it to reload the list more often and waiting
     5s for the buttons to return is a pain.

   2022-11-21:
   - updating selectors; there was another update a few weeks on 2022-10-05 ago
     but I didn't commit it :(

   2022-10-05:
   - updating selectors yet again.

   2022-08-21:
   - broke again
   - changed a bunch of selectors

   2022-06-15:
   - changed structure affecting chat_name_sel.  We now have a bunch of intermediary DIVs
     between chat labels and the aria-label='Chats' DIV.  Now we look for its DIV descendents
     with data-testid='mwthreadlist-item-open' :)

   2022-01-23:
   - they changed the structure affecting chat_name_sel.  No longer three nested span elements.
   - also changed classes that allowed me to select chats; changing to using [aria-label='Chats'], yay

   2021-12-08:
   - they changed the label from "Archive Chat" to "Archive chat"
   - they changed the selector for chat_elem_sel again
   - fix my own syntax error in a variable name

   2021-04-15:
   - they changed the label from "Hide Conversation" to "Archive Chat"

   2021-03-22:
   - stopped working again.  Apparently the chat_elem_sel dropped a div now? :shrug:  Updated, and added some logging.

   2020-12-24:
   - Facebook recently updated the coding for messenger.com and facebook.com/messages; had to update selectors
   - made the Hide link into a button
   - would like to figure out how to set-up the hide event myself to by-passing going through the UI actions
   - would like to stop polling for new chats, and instead wait for new chats to join the list event-based style

   ==Error debugging==

   Exceptions caught by GreaseMonkey won't show up in a tab's console; need to
   check the browser console.  First, open a tab's console, and then press
   ctrl-shift-j to pop open the browser console.

   ==Usage==
   Either as a Bookmarklet vs GreaseMonkey script.
 */

interface IntervalCount {
  value: number;
}

/* CSS Selectors for key UI elements */
const chat_elem_sel = "div[aria-label='Chats'] > div > div > div.xs83m0k > div > div.x1n2onr6 > div.xexx8yu";
const chat_name_sel = "div.x9f619 > div > span.x193iq5w > span.x1lliihq";

const action_menu_sel       = "div[aria-label='Menu']";
const action_menu_items_sel = "div.x1i10hfl > div.x6s0dn4 > div.x78zum5 > div.xu06os2 > span.x193iq5w";
const action_menu_outer_sel = "div.x1uvtmcs"; /* outer-most DIV for the action popup menu */

/* interval settings */
const hide_click_interval_max =   15; /* # of attempts to make */
const hide_click_interval     =  100; /* ms */
const add_buttons_interval    = 2000; /* 5s */

/* counters, for error handling */
let g_intervals_cnt          = 0;
let g_chat_elems_found_cnt   = 0;
let g_chat_elems_applied_cnt = 0;

function fbm_error (err_msg: string) {
  console.log (`ERROR: ${err_msg}`);
  window.alert (`ERROR: ${err_msg}`);
}

/* fbm_hide_chat_by_index:
 *
 * Hide a specific chat at a given index; mostly for testing/debugging.
 *
 * doc: the HTML Document holding the chat list.
 * chat_index: Index of the chat in the list (top one is 0, next is 1, etc)
 */
function fbm_hide_chat_by_index (doc: Document, chat_index: number) {
  let selector = chat_elem_sel + ":nth-child (" + chat_index + ")";

  let chat_li = doc.querySelector (selector) as HTMLElement;
  if (chat_li) {
    fbm_hide_chat (doc, chat_li);
  } else {
    console.debug (`Could not find chat using selector '${selector}' for index ${chat_index}`);
  }
}

/* fbm_click_hide_button:
 *
 * Once fbm_hide_chat event-handler has been called, it loads the action menu
 * for a chat and then needs to wait for the 'Hide Conversation'
 * to load to trigger it.
 */
function fbm_click_hide_button (interval_cnt: IntervalCount, interval_id: number, doc: Document, chat_name: string): boolean {
  /* NOTE: this is run on an interval for a max number of times before giving up, as it may take time for the menu to appear */
  if (interval_cnt.value >= hide_click_interval_max /* 10 */) {
    console.warn ("WARNING: Chat list did not load after " + interval_cnt.value + " intervals.  Aborting");
    clearInterval (interval_id);
    return false;
  } else {
    interval_cnt.value++;
  }

  let anchors = doc.querySelectorAll (action_menu_items_sel) as NodeListOf<HTMLElement>;
  if (anchors.length == 0) {
    console.debug (`NOTE: ${interval_id}:${interval_cnt.value}/${hide_click_interval_max}: No action menu anchors found using selector '${action_menu_items_sel}' for chat with '${chat_name}'.  Trying again.`);
    return false;
  }

  let hide_anchor = null;
  for (let i = 0; i < anchors.length; i++) {
    if (anchors[i].innerText == "Hide Conversation" || anchors[i].innerText == "Archive Chat" || anchors[i].textContent == "Archive chat") {
      console.debug (anchors[i].innerText);
      hide_anchor = anchors[i];
      break;
    }
  }

  if (hide_anchor == null) {
    fbm_error (`Couldn't find 'Hide' anchor for chat with '${chat_name}', aborting.`);
    return false;
  }

  hide_anchor.click ();
  clearInterval (interval_id);

  return true;
}

/* fbm_hide_chat:
 * event handler for clicks on the "Hide" button, that will hide the
 * corresponding chat's LI
 *
 * doc: The HTML Document, necessary to work within the correct frame (boo, frames)
 * chat_elem: The HTML element representing an individual chat in the left-hand side chat list.
 */
function fbm_hide_chat (doc: Document, chat_elem: HTMLElement) {
  if (chat_elem.tagName != "DIV") {
    fbm_error (`Did not get a chat DIV, aborting; instead found tagName '${chat_elem.tagName}'`);
    return;
  }

  const name_span = chat_elem.querySelector (chat_name_sel) as HTMLElement;
  if (!name_span) {
    fbm_error ("Could not find name span, aborting.");
    return;
  }
  let chat_name = name_span.innerHTML;

  console.debug (`Hiding chat for '${chat_name}'`);

  const action_div = chat_elem.querySelector (action_menu_sel) as HTMLElement;

  if (!action_div) {
    fbm_error (`Couldn't find 'Conversation actions' div where expected for chat with '${chat_name}'.  Aborting hide.`);
    return;
  }

  if (action_div.getAttribute ("aria-label") != "Menu") {
    fbm_error (`Actions DIV didn't have expected aria-label; `+
      `expected 'Menu' but found '${action_div.getAttribute ("aria-label")}'.`+
      `For chat '${chat_name}'.  Aborting hide.`);
    return;
  }

  action_div.click (); /* create chat's action menu */

  const interval_cnt: IntervalCount = { value: 0 };
  const interval_id = setInterval (function () {
    if (fbm_click_hide_button (interval_cnt, interval_id, doc, chat_name)) {
      chat_elem.style.display = "none"; /* pre-emptively hide the DIV; risky if clicking fails; it
                                           will re-appear next reload on failure; smoother experience
                                           for when it works, though.  */
    }
  }, hide_click_interval_max /* 100ms */);
}

/* fbm_add_hide_button:
 *
 * Iterates through all chats and adds a clickable Hide button in the top-right of each.
 *
 * doc: The HTML document that the chats are located on, necessary to deal with frames.
 */
function fbm_add_hide_buttons_to_doc (doc: Document) {
  /* console.log ("fbm_add_hide_button:"); */
  if (!doc) {
    return;
  }

  let chat_elems = doc.querySelectorAll (chat_elem_sel + ":not(.chat_hide_added)") as NodeListOf<HTMLElement>;

  if (chat_elems.length > 0) {
    console.debug (`Found ${chat_elems.length} chat LIs...`);

    g_chat_elems_found_cnt += chat_elems.length;

    for (let i = 0; i < chat_elems.length; i++) {
      let chat_elem = chat_elems[i];

      let chat_name_elem = chat_elem.querySelector (chat_name_sel);
      if (!chat_name_elem) {
        console.warn (`WARNING: chat elem LI ${i} using selector '${chat_name_sel}' found no matching element for chat name.  Skipping. chat_elem: `, chat_elem);
        continue;
      }
      let chat_name = chat_name_elem.innerHTML;
      if (!chat_name) {
        console.warn (`WARNING: chat name elem ${i} from selector '${chat_name_sel}' has no innerHTML.  Skipping.`);
        continue;
      }

      let hide_a = chat_elem.querySelector ("a.hide_a") as HTMLElement;
      if (hide_a == null) {
        hide_a = doc.createElement ("A");
        hide_a.classList.add ("hide_a");

        hide_a.innerHTML = "<div style='border: 1px solid black; background-color: #444; padding: 2px; width: 3em; text-align: center;'>Hide</div>";

        hide_a.style.color = "white";
        hide_a.style.position = "absolute";
        hide_a.style.right = "0px";
        hide_a.style.zIndex = "999";

        hide_a.onclick = function (ev) {
          console.debug ("Running Hide on: " + chat_name);
          fbm_hide_chat (doc, chat_elem);
        };
        chat_elem.insertBefore (hide_a, chat_elem.firstChild);
        console.debug ("Adding 'Hide' button to chat for " + chat_name);
        chat_elem.className += " chat_hide_added";

        g_chat_elems_applied_cnt++;
      } else {
        /* console.log (chat_name + " already has a hide button"); */
      }
    }
  }
}

/* fbm_add_hide_buttons:
 *
 * Attempts to add the hide buttons to both the main document and to the documents of any frames.
 */
function fbm_add_hide_buttons_to_docs (docs: Document[]) {
  for (let i = 0; i < docs.length; i++) {
    let doc = docs[i];
    fbm_add_hide_buttons_to_doc (doc);
  }
}

function fbm_delay_action_menu_display () {
  const style = document.createElement ("STYLE");
  style.innerHTML = `
      ${action_menu_outer_sel} {
        animation-name: delayfadein;
        animation-duration: 0.33s;
      }
      @keyframes delayfadein {
          0% { opacity: 0; }
         99% { opacity: 0; }
        100% { opacity: 1; }
      }
    `;
  document.head.appendChild (style);
}

/* fbm_main_loop:
 *
 * Once the page has loaded, add Hide buttons to present chats, checking every
 * 5s for new ones.
 */
function fbm_main_loop () {
  fbm_delay_action_menu_display ();

  let docs: Document[] = [document];
  for (let i = 0; i < window.frames.length; i++) {
    try {
      docs.push (window.frames[i].document);
    } catch (e: unknown) {
      console.debug (`Frame: ${i} access error: ` + (e as Error).message);
    }
  }

  if (docs.length == 0) {
    fbm_error ("No documents found within available window.frames");
    return;
  }

  window.addEventListener ("load", function () {
    setInterval (function () {
      fbm_add_hide_buttons_to_docs (docs);

      if (g_chat_elems_applied_cnt == 0) {
        if (g_intervals_cnt == 0) {
          console.log ("fbmh: No chat elements had button added on first past; waiting another " +
            (add_buttons_interval / 1000) + " seconds");
        } else if (g_intervals_cnt == 1) {
          if (g_chat_elems_found_cnt == 0) {
            console.warn (`fbmh: WARNING: no chats found yet.  Selector '${chat_elem_sel}' may no longer be applicable.`);
          } else {
            console.warn (`fbmh: WARNING: ${g_chat_elems_found_cnt} chats found, but 0 have had a button added.`);
          }
        }
      }

      g_intervals_cnt++;
    }, add_buttons_interval);
  }, false);
}

console.log ("fbmhide: FBM Add Hide Chat button loaded");
fbm_main_loop ();
