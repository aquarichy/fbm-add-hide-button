all: build/fbm-hide.js

build/fbm-hide.js: src/fbm-hide.ts
	tsc

clean:
	find . -name "*~" -delete -or -name "#*#" -delete
	rm -f build/rbm-hide.js
	rmdir build || true
