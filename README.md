# Facebook Messenger Hide Chat Button

This adds a "Hide" button to each conversation contact in the left-hand side bar
of messenger.com and facebook.com/messenger, as a short-cut to the Hide
Conversation action in the action menu.

Occasionally, the CSS selectors used to access the action menu, chat name, and
hide button change when Facebook updates their code, breaking this script.

In the future, it might be nice to run the JavaScript to hide a conversation
directly.  Right now, this simulates clicks to achieve that.

## License

GPLv3

## Usage

This can either be used as a GreaseMonkey script or as a bookmarklet.

## Help

aquarichy (at) gmail (dot) com